# ansible-remarkable
Manage my remarkable

## Setup
Current Remarkable Version: 2.5.0.27

Optional: Install [remarkable-hacks](https://github.com/ddvk/remarkable-hacks)

Required:
* Install [remarkable_entware](https://github.com/evidlo/remarkable_entware)
* Install python3 via opkg on remarkable

Locally I am currently using **Python 3.9.0** and a virtualenv with **ansible 2.10.5**

## TODO
* Create Dayplanner template
* Create ansible role to manage templates
* Create splash screens

